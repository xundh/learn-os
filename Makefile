TOOLPATH 	= ./tools/
TARGET      = ./target/
SRC    		= ./src/
LD			=./env/bin/i686-elf-ld.exe
CC			=./env/bin/i686-elf-gcc.exe
NASM		=$(TOOLPATH)nasm.exe
DD			=$(TOOLPATH)dd.exe
BOCHSPATH	=$(TOOLPATH)bochs/

MAKE     	=$(TOOLPATH)make.exe -r
COPY	 	= copy
DEL      	= del

# LD编译选项
LDFLAGS		=-m elf_i386
# GCC编译选项
CCFLAGS		=-march=i386 -m16 -mpreferred-stack-boundary=2 -ffreestanding
ASFLAGS		=

# 默认动作
default :  kernel.bin  loader.bin kernel.o utilities.o loader.o

# 汇编的函数 用来给C调用
utilities.o: $(SRC)kernel/utilities.s
	$(NASM) $(ASFLAGS) -f elf32 -o $(TARGET)$@ $^

kernel.o : $(SRC)kernel/kernel.c $(SRC)kernel/utilities.h
	$(CC) $(CCFLAGS) -o $(TARGET)kernel.o -c $(SRC)kernel/kernel.c

kernel.bin : kernel.o utilities.o
	$(LD) $(LDFLAGS) -Ttext 0xC200 --oformat binary -o $(TARGET)$@ $(TARGET)kernel.o $(TARGET)utilities.o

loader.o: $(SRC)boot/loader.s
	$(NASM) $(ASFLAGS) -f elf32 -o $(TARGET)$@ $^

loader.bin : loader.o
	$(LD) $(LDFLAGS) -Ttext 0x7c00 --oformat binary -o $(TARGET)$@ $(TARGET)$^

install :
	$(DD) if=$(TARGET)kernel.bin of=$(TARGET)os.img conv=notrunc bs=512 seek=1
	$(DD) if=$(TARGET)loader.bin of=$(TARGET)os.img conv=notrunc
  
run : Makefile
	cd $(TOOLPATH) && bochs.bat && cd ..

debug:
	cd $(TOOLPATH) && bochsdebug.bat && cd ..
	
clean :
	cd $(TARGET) && del -f *