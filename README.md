﻿# LeranOS
---------------------

## 一、介绍
通过自制来学习操作系统知识，参考资料：
 - 《30天自制操作系统》
 - 《自己动手写操作系统》两本书。
 - https://www.cnblogs.com/flashsun/p/12218813.html

本人c和汇编、操作系统都是菜鸟，如发现错误欢迎大家指正。

## 二、开发过程
### 1. 目录结构

target               编译目标
tools                一些工具软件
env                  不同环境编译工具
src
 |--boot                 引导区
      |--boot.s          引导程序
      |--head.s          启动程序
 |--kernel
      |--kernel.c        C语言入口程序
      |--utilities.h     汇编子程序头文件
      |--utilities.s     汇编子程序


## 三、编译运行
### 1. 准备工作
 - windows7             在Windows下开发
 - 把 tools/ tools/bochs 两个文件夹加到系统Path变量。

### 2. 启动命令行编译
```bash
make 
make install
make run          # 启动Bochs运行镜像程序
make debug        # 进入Bochs调试状态
```

