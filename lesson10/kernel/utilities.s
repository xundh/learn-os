[BITS 16]
global io_hlt
global clear_screen

clear_screen:
    ; 清屏
    mov ax, 03h
    int 10h
	RET

io_hlt:
	HLT
	RET