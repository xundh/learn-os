
#include "utilities.h"

int strlen(char* p);

void main(void){
    clear_screen(); // clear screen
    char* str="NotOneOS Console . \n Welcome.. \n \0";
    int   len=strlen(str);
    print_str(str, len);
    char a ;
    fin:
        a = read_char();
        print_char(a);
        goto fin;
}

/**
 * 计算字符串长度
 */
int strlen(char* p){
    int count=0;
    while(*p++ )count++;
    return count;
}
