[BITS 16]
global io_hlt
global clear_screen
global read_char;
global print_char;
global print_str;
global shutdown;


; 清屏
clear_screen:
    MOV ax, 03h
    INT 10h
	RET

; 系统挂起
io_hlt:
	HLT
	RET

; 读一个字符到在AL中
read_char:
    MOV ah,0x00
    INT 0x16
    RET

; 打印AL中的字符
print_char:
    MOV AL,[esp+4]
print_char_al:
    MOV AH,0x0e
    INT 0x10
    CMP al, 0x0d    ; 是不是回车
    JNE not_cr      ; 不是
    MOV al, 0x0a    ; 输出换行
    INT 0x10        ; output the LF 
not_cr: 
    RET


; 打印字符串的子程序，这里没有使用13H的BIOS子功能，而是使用循环显示单个字符的方法
print_str:
    MOV edx,[esp+8]         ;len
    MOV SI,[esp+4]          ;msg
print:
    MOV      AL, [SI]
    ADD      SI, 1
    CMP      AL, 0          ; 字符串有没有读完，到0结束
    DEC      EDX
    JE       end_print      ; 跳到结束程序
    MOV      AH, 0x0e       ; 显示一个字符
    MOV      BX, 15         ; 指定颜色
    INT      0x10           ; 调用BIOS功能显示字符
    CMP      AL, 0x0d
    JNE      not_cr_0
    MOV      AL, 0x0a       ; 输出换行
    INT      0x10
    MOV      BH, 0x00       ; 下面要把光标移到头部，但没起作用，以后再看是怎么回事
    MOV      AH, 0x02
    MOV      DH, 0x01
    MOV      DL, 0x01
    INT      0x10

not_cr_0:
    JMP      print
end_print:
   RET

; 关机
shutdown:
    MOV AX, 5301H        ;Function 5301h: APM Connect real-mode interface
    XOR BX, BX               ;Device ID: 0000h (=system BIOS)
    INT 15H                      ;Call interrupt: 15h

    MOV AX, 530EH         ;Function 530Eh: APM Driver version
    MOV CX, 0102H         ;Driver version: APM v1.2
    INT 15H                       ;Call interrupt: 15h

    MOV AX, 5307H        ;Function 5307h: APM Set system power state
    MOV BL, 01H            ;Device ID: 0001h (=All devices)
    MOV CX, 0003H        ;Power State: 0003h (=Off)
    INT 15H                       ;Call interrupt: 15h
    RET
