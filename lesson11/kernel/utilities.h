#ifndef __UTILITIES_H_
#define __UTILITIES_H_
// 此处只提供函数声明
void io_hlt(void);
void clear_screen(void);
char read_char(void);
void print_char(char);
void print_str(char *msg,int len);
#endif