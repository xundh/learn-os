[BITS 16]
ORG     0xc200
entry:
   ; 清屏
   MOV AX, 03h
   INT 10h

   ; 显示正在加载
   MOV      SI , msg  

print:
   MOV      AL, [SI]
   ADD      SI, 1
   CMP      AL, 0          ; 字符串有没有读完，到0结束
   JE       end            ; 跳到结束程序
   MOV      AH, 0x0e       ; 显示一个字符
   MOV      BX, 15         ; 指定颜色
   INT      0x10           ; 调用BIOS功能显示字符
   JMP      print

end:
   HLT
   jmp      end              ; 无限循环停止程序

msg:
		DB		0x0a, 0x0a		; 换行两次
		DB		"Loading boot.sys..."
		DB		0x0a			; 换行
		DB		0
